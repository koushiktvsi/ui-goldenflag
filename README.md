/-------- Top Left Notification ------------/
1. Info icon shows all the notification like New Call, Completed Task, Callback request etc
2. Comment icon shows comments given by the customer to the service provided

/--------- Top Right Menu ------------/
Scheduler - On click, takes to Scheduler section
Order History - On click, takes to order history section

/----------- Boxes ------------/
Boxes in the home page shows numbers which are of importance

/----------- Scheduler Section ------------/
Unassigned Task - This box shows tasks which hasn't been assigned to anyone. The task can be dragged directly in technicians timeline.
P.S. Same task can be assigned to multiple technicians. For eg, if a house needs painting, multiple painter can work on the same task.
Note: Remove after drop has to be checked to assign a call to a single technician

Each category can be expanded to see the list of technician and their timeline. Second column shows the number of task which was taken by that technician 
in the last 30 days. Followed by timeline.

Calendar can be viewed on daily, 3 days, weekly, and monthly.

/----------- Order History ------------/
- Dropdown option shows 10, 25, 50, and All rows
- Order History table can be exported to Excel or Pdf. -------Doesn't work----
- From and To date can be selected to view the history. ------Doesn't work----
- Any data can be searched from the search box
- Clicking the arrows in column header arranges the data in ascending or descending

/---------------------------------------------------------------------------------------------------------------------------/
								Time Line Page
/---------------------------------------------------------------------------------------------------------------------------/
Note: This is active for the technician named "Bill". To view this, search "PL1234" in Order History table. In the only result you will get,
with this Order ID is already assigned to Bill. Clicking on Bill will open a new page with his timeline. At the moment, there's no option to view 
timeline with From to To date.

/-------------------Profile Section---------------------/
Here basic information of the technician is displayed

/-------------------Timeline Section---------------------/
This section shows tasks activities from latest. Assigned and Callback task falls in left, while the completed task falls in right with order id

/-------------------Comment Section---------------------/
Here comments received by the customer can be viewed.