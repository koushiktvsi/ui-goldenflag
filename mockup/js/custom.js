//**********Scheduler**************//

$(document).ready(function(){
	$(function() { // document ready

	/* initialize the external events
		-----------------------------------------------------------------*/

		$('#external-events .fc-event').each(function() {

			// store data so the calendar knows to render an event upon drop
			$(this).data('event', {
				title: $.trim($(this).text()), // use the element's text as the event title
				stick: true // maintain when user navigates (see docs on the renderEvent method)
			});

			// make the event draggable using jQuery UI
			$(this).draggable({
				zIndex: 999,
				revert: true,      // will cause the event to go back to its
				revertDuration: 0  //  original position after the drag
			});

		});
	
	
	/* initialize the calendar
		-----------------------------------------------------------------*/
		$('#calendar').fullCalendar({
			now: '2016-03-11',
			editable: true,
			droppable: true, // this allows things to be dropped onto the calendar
			aspectRatio: 1.8,
			scrollTime: '00:00',
			header: {
				left: 'today prev,next',
				center: 'title',
				right: 'timelineDay,timelineThreeDays,agendaWeek,month'
			},
			defaultView: 'timelineDay',
			views: {
				timelineThreeDays: {
					type: 'timeline',
					duration: { days: 3 }
				}
			},
			resourceAreaWidth: '25%',
			resourceGroupField: 'building',
			resourceColumns: [
				
				{
					labelText: 'Technician',
					field: 'title'
				},
				{
					labelText: 'Task in 30d',
					field: 'task'
				}
			],
			resources: [
				{ id: 'a', building: 'Plumbing', title: 'Bill', task: 40 },
				{ id: 'b', building: 'Plumbing', title: 'Matt', task: 40, eventColor: 'green' },
				{ id: 'c', building: 'Plumbing', title: 'Mike', task: 40, eventColor: 'orange' },
				{ id: 'd', building: 'Plumbing', title: 'John', task: 40,},
				{ id: 'e', building: 'Plumbing', title: 'Karolin', task: 40 },
				{ id: 'f', building: 'Plumbing', title: 'Christina', task: 40, eventColor: 'red' },
				{ id: 'g', building: 'Electrical', title: 'Steve', task: 40 },
				{ id: 'h', building: 'Electrical', title: 'Robert', task: 40 },
				{ id: 'i', building: 'Electrical', title: 'Jennifer', task: 40 },
				{ id: 'j', building: 'Electrical', title: 'Leo', task: 40 },
				{ id: 'k', building: 'Electrical', title: 'Brukes', task: 40 },
				{ id: 'l', building: 'Makeup', title: 'Katherin', task: 40 },
				{ id: 'm', building: 'Makeup', title: 'Shawn', task: 40 },
				{ id: 'n', building: 'Makeup', title: 'Maria', task: 40 },
				{ id: 'o', building: 'Makeup', title: 'Linda', task: 40 },
				{ id: 'p', building: 'Makeup', title: 'Leslie', task: 40 },
				{ id: 'q', building: 'Painting', title: 'Louis', task: 40 },
				{ id: 'r', building: 'Painting', title: 'Sean', task: 40 },
				{ id: 's', building: 'Painting', title: 'Mandy', task: 40 },
				{ id: 't', building: 'Pest Control', title: 'Alan', task: 40 },
				{ id: 'u', building: 'Pest Control', title: 'Tommy', task: 40 },
				{ id: 'v', building: 'Pest Control', title: 'Perlis', task: 40 },
				{ id: 'w', building: 'Pest Control', title: 'Robert', task: 40 },
				{ id: 'x', building: 'Gardening', title: 'Abraham', task: 40 },
				{ id: 'y', building: 'Gardening', title: 'Joseph', task: 40 },
				{ id: 'z', building: 'Gardening', title: 'Bucky', task: 40 }
			],
			events: [
				{ id: '1', resourceId: 'b', start: '2016-03-11T02:00:00', end: '2016-03-11T07:00:00', title: 'PL0001' },
				{ id: '2', resourceId: 'i', start: '2016-03-11T05:00:00', end: '2016-03-12T22:00:00', title: 'EL0001' },
				{ id: '3', resourceId: 'l', start: '2016-03-11T02:00:00', end: '2016-03-11T04:00:00', title: 'MU0011' },
				{ id: '4', resourceId: 'r', start: '2016-03-11T03:00:00', end: '2016-03-15T08:00:00', title: 'PT0018' },
				{ id: '5', resourceId: 'v', start: '2016-03-11T00:30:00', end: '2016-03-12T02:30:00', title: 'PC0005' },
				{ id: '5', resourceId: 'y', start: '2016-03-11T00:30:00', end: '2016-03-12T02:30:00', title: 'GR0040' }
			],
		drop: function(date, jsEvent, ui, resourceId) {
				console.log('drop', date.format(), resourceId);

				// is the "remove after drop" checkbox checked?
				if ($('#drop-remove').is(':checked')) {
					// if so, remove the element from the "Draggable Events" list
					$(this).remove();
				}
			},
			eventReceive: function(event) { // called when a proper external event is dropped
				console.log('eventReceive', event);
			},
			eventDrop: function(event) { // called when an event (already on the calendar) is moved
				console.log('eventDrop', event);
			}
		});

	});
});


// Counter
$(document).ready(function(){
	$('.count').each(function () {
		$(this).prop('Counter',0).animate({
			Counter: $(this).text()
		}, 
		{
        duration: 4000,
        easing: 'swing',
        step: function (now) {
            $(this).text(Math.ceil(now));
			}
		});
	});
});




//Text Color in Order History
$(document).ready(function(){
	$('td:contains("Assigned")').css('color','#3498db');
	$('td:contains("Unassigned")').css('color','#e67e22');
	$('td:contains("Completed")').css('color','#2ecc71');
	$('td:contains("Callback")').css('color','#e74c3c');
});

//Ratings
$(document).ready(function(){
	$(function () {
        $('input.check').on('change', function () {
          alert('Rating: ' + $(this).val());
        });
        $('#programmatically-set').click(function () {
          $('#programmatically-rating').rating('rate', $('#programmatically-value').val());
        });
        $('#programmatically-get').click(function () {
          alert($('#programmatically-rating').rating('rate'));
        });
        $('.rating-tooltip').rating({
          extendSymbol: function (rate) {
            $(this).tooltip({
              container: 'body',
              placement: 'bottom',
              title: 'Rate ' + rate
            });
          }
        });
        $('.rating-tooltip-manual').rating({
          extendSymbol: function () {
            var title;
            $(this).tooltip({
              container: 'body',
              placement: 'bottom',
              trigger: 'manual',
              title: function () {
                return title;
              }
            });
            $(this).on('rating.rateenter', function (e, rate) {
              title = rate;
              $(this).tooltip('show');
            })
            .on('rating.rateleave', function () {
              $(this).tooltip('hide');
            });
          }
        });
        $('.rating').each(function () {
          $('<span class="label label-default"></span>')
            .text($(this).val() || ' ')
            .insertAfter(this);
        });
        $('.rating').on('change', function () {
          $(this).next('.label').text($(this).val());
        });
      });
});